/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,ts}',  "./node_modules/flowbite/**/*.js"],
  theme: {
    extend: {
      colors: {
        "violetish": "var--(main-violet)",
        "whitish": "var--(whitish)"
      }
    }
  },
  plugins: [require('flowbite/plugin')]
};

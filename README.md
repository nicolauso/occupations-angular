# Ressources

## Backlog
https://trello.com/b/hep2MBC7/occupations

## Maquettes
https://www.figma.com/file/U4lHkfj4tKf1gvI1rJqA4M/OCCupations?type=design&mode=design&t=UPdiiF5vdKGBtIZA-0

## Schémas
https://drive.google.com/file/d/1cGzV1aapQg5eizgEUmyRVodfw7L3DaQG/view

## API Tierce Data Gouv
https://data.toulouse-metropole.fr/explore/dataset/agenda-des-manifestations-culturelles-so-toulouse/api/

export type Tab = {
  id: number;
  icon: string;
  title: string;
  path: string;
};

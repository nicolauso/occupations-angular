import { APIService } from './api.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Event } from '../interfaces/event';

@Injectable({
  providedIn: 'root'
})
export class SearchFiltersService {
  currentDate: Date = new Date();

  constructor(
    private APIService: APIService,
    private httpClient: HttpClient
  ) {}

  isFree: boolean = false;
  city: string = '';
  category: string = '';
  startDate: string = this.currentDate.toLocaleString();
  endDate: string = this.currentDate.toLocaleString();

  // isFree filter methods
  getIsFree() {
    return this.isFree;
  }
  setIsFree(value: boolean) {
    this.isFree = value;
  }

  // city filter methods
  getCity() {
    return this.city;
  }
  setCity(value: string) {
    this.city = value;
  }
  // category filter methods
  getCategory() {
    return this.category;
  }
  setCategory(value: string) {
    this.category = value;
  }
  // startDate filter methods
  getStartDate() {
    return this.startDate;
  }
  setStartDate(value: string) {
    this.startDate = value;
  }
  // endDate filter methods
  getEndDate() {
    return this.endDate;
  }
  setEndDate(value: string) {
    this.endDate = value;
  }

  URL_API = `https://data.toulouse-metropole.fr/api/explore/v2.1/catalog/datasets/agenda-des-manifestations-culturelles-so-toulouse/where=commune=${this.city}/records?limit=20`;

  getFilteredEvents(): Observable<EventList> {
    console.log(
      'getFilteredEvents',
      this.httpClient.get<EventList>(this.URL_API)
    );

    return this.httpClient.get<EventList>(this.URL_API);
  }
}

type EventList = {
  total_count: number;
  results: Event[];
};

// FILTRES
// date_debut: string;
// date_fin: string;
// code_postal: number;
// commune: string;
// categorie_de_la_manifestation: string;
// manifestation_gratuite: boolean | null;

// identifiant: string;
// nom_generique: string | null;
// nom_de_la_manifestation: string;
// descriptif_court: string;
// descriptif_long: string;

// horaires: string | null;
// dates_affichage_horaires: string;
// modification_derniere_minute: string | null;
// lieu_nom: string;
// lieu_adresse_1: string | null;
// lieu_adresse_2: string | null;
// lieu_adresse_3: string | null;

// type_de_manifestation: string;

// theme_de_la_manifestation: string | null;
// station_metro_tram_a_proximite: string | null;
// googlemap_latitude: number;
// googlemap_longitude: number;
// reservation_telephone: string | null;
// reservation_email: string | null;
// reservation_site_internet: string | null;

// tarif_normal: string | null;
// tarif_enfant: string | null;
// tranche_age_enfant: string | null;
// geo_point: GeoPoint;

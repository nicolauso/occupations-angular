import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Event } from '../interfaces/event';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class APIService {
  URL_API =
    'https://data.toulouse-metropole.fr/api/explore/v2.1/catalog/datasets/agenda-des-manifestations-culturelles-so-toulouse/records?limit=20';

  constructor(private httpClient: HttpClient) {}

  getAllEvents(): Observable<EventList> {
    return this.httpClient.get<EventList>(this.URL_API);
  }
}

type EventList = {
  total_count: number;
  results: Event[];
};

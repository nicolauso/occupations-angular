import { Injectable } from '@angular/core';
import { Event } from '../interfaces/event';

@Injectable({
  providedIn: 'root'
})
export class FavoriteEventsService {
  favoriteEvents: Event[] = [];

  constructor() {}

  addFavoriteEvent(event: Event) {
    this.favoriteEvents.push(event);
    console.log(this.favoriteEvents);
  }

  removeFavoriteEvent(event: Event) {
    this.favoriteEvents = this.favoriteEvents.filter(
      (currentEvent) => currentEvent != event
    );
  }

  getFavoriteEvents(): Event[] {
    return this.favoriteEvents;
  }

  isFavorite(event: Event): boolean {
    return this.favoriteEvents.find((eachEvent)=>eachEvent.identifiant === event.identifiant) !== undefined;
  }
}

import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { MyEventsComponent } from './components/my-events/my-events.component';
import { ProfileComponent } from './components/profile/profile.component';

export const routes: Routes = [
  { path: 'my-events', component: MyEventsComponent },
  { path: '', component: HomeComponent },
  { path: 'profile', component: ProfileComponent }
  // { path: '**', component: PageNotFoundComponent }
];

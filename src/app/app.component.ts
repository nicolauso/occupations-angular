import { SearchComponent } from './components/search/search.component';
import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { NavbarMobileComponent } from './components/navbar-mobile/navbar-mobile.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './components/home/home.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    SearchComponent,
    CommonModule,
    FormsModule,
    NavbarMobileComponent,
    HomeComponent,
    LeafletModule
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'angular-template-lite';
}

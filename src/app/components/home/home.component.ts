import { Component, OnInit } from '@angular/core';
import { APIService } from '../../services/api.service';
import { ModalSearchComponent } from '../modal-search/modal-search.component';
import { HeaderComponent } from '../header/header.component';
import { CardComponent } from '../card/card.component';
import { Event } from '../../interfaces/event';
import { CardListComponent } from '../card-list/card-list.component';
import { FavoriteEventsService } from '../../services/favorite-events.service';
import { CommonModule } from '@angular/common';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { latLng, tileLayer } from 'leaflet';
import { ToggleMapComponent } from '../toggle-map/toggle-map.component';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    ModalSearchComponent,
    HeaderComponent,
    CardComponent,
    CardListComponent,
    CommonModule,
    LeafletModule,
    ToggleMapComponent
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent implements OnInit {
  events: Event[] = [];
  filterModalIsOpen: boolean = false;
  options = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&amp;copy; OpenStreetMap contributors'
      })
    ],
    zoom: 7,
    center: latLng([46.879966, -121.726909])
  };

  constructor(
    private APIService: APIService,
    private favoriteService: FavoriteEventsService
  ) {}

  ngOnInit() {
    this.APIService.getAllEvents().subscribe({
      next: (res) => {
        console.log(res);
        this.events = res.results;
      },
      error: (err) => console.error('Request error: ' + err),
      complete: () => console.log(this.events)
    });
  }

  setfilterModalIsOpen() {
    this.filterModalIsOpen = !this.filterModalIsOpen;
  }
}

import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Event } from '../../interfaces/event';
import { FavoriteEventsService } from '../../services/favorite-events.service';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './card.component.html',
  styleUrl: './card.component.css'
})

export class CardComponent {
  isExpanded = false;
  isNotificated:boolean = false;

  constructor(private favoriteService: FavoriteEventsService) {}

  ngOnInit(){
    this.isNotificated = this.favoriteService.isFavorite(this.cardDetails)
  }

  toggleCard() {
    this.isExpanded = !this.isExpanded;
  }

  toggleNotification() {
    if (this.isNotificated) {
      this.favoriteService.removeFavoriteEvent(this.cardDetails);
    } else {
      this.favoriteService.addFavoriteEvent(this.cardDetails);
    };

    this.isNotificated = !this.isNotificated;
  }
  @Input() cardDetails!: Event;
}

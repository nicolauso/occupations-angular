import { SearchFiltersService } from '../../services/search-filters.service';
import { Component, OnInit } from '@angular/core';
import { ToggleFreeComponent } from '../toggle-free/toggle-free.component';
import { DatePickerComponent } from '../date-picker/date-picker.component';
import { SearchBarComponent } from '../search-bar/search-bar.component';
import { FilterCityComponent } from '../filter-city/filter-city.component';
import { FilterCategoryComponent } from '../filter-category/filter-category.component';
@Component({
  selector: 'app-search',
  standalone: true,
  imports: [
    ToggleFreeComponent,
    DatePickerComponent,
    SearchBarComponent,
    FilterCityComponent,
    FilterCategoryComponent
  ],
  templateUrl: './search.component.html',
  styleUrl: './search.component.css'
})
export class SearchComponent implements OnInit {
  constructor(private SearchFiltersService: SearchFiltersService) {}
  isFree!: boolean;
  city!: string;
  category!: string;
  startDate!: string;
  endDate!: string;

  ngOnInit() {
    console.log('initialization search', this.SearchFiltersService.getCity());
    this.isFree = this.SearchFiltersService.getIsFree();
    this.city = this.SearchFiltersService.getCity();
    this.category = this.SearchFiltersService.getCategory();
    this.startDate = this.SearchFiltersService.getStartDate();
    this.endDate = this.SearchFiltersService.getEndDate();
  }
  // city free method
  setIsFree() {
    return this.SearchFiltersService.setIsFree(!this.isFree);
  }
  // city filter method
  setCity(value: string) {
    return this.SearchFiltersService.setCity(value);
  }

  // category filter method
  setCategory(value: string) {
    return this.SearchFiltersService.setCategory(value);
  }

  // startDate filter method
  setStartDate(value: string) {
    return this.SearchFiltersService.setStartDate(value);
  }

  // endDate filter method
  setEndDate(value: string) {
    return this.SearchFiltersService.setEndDate(value);
  }

  getFilteredEventsAPI() {
    return this.SearchFiltersService.getFilteredEvents();
  }
}

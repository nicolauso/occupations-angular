import { Component } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { Input } from '@angular/core';
import { SearchFiltersService } from '../../services/search-filters.service';

@Component({
  selector: 'app-filter-city',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './filter-city.component.html',
  styleUrl: './filter-city.component.css'
})
export class FilterCityComponent {
  @Input() city!: string;

  //cityValue = new FormControl<string>('');

  arrayOfCities = [
    { value: 'NY', title: 'New-York' },
    { value: 'TLSE', title: 'Toulouse' },
    { value: 'PERPG', title: 'Perpignan' }
  ];

  setCity(event: Event) {
    this.SearchFiltersService.setCity(
      (event.target as HTMLSelectElement).value
    );
  }

  constructor(public SearchFiltersService: SearchFiltersService) {}
}

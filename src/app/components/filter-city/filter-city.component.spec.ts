import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterCityComponent } from './filter-city.component';

describe('FilterCityComponent', () => {
  let component: FilterCityComponent;
  let fixture: ComponentFixture<FilterCityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FilterCityComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FilterCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchFiltersService } from '../../services/search-filters.service';

@Component({
  selector: 'app-filter-category',
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule],
  templateUrl: './filter-category.component.html',
  styleUrl: './filter-category.component.css'
})
export class FilterCategoryComponent {
  @Input() category?: string;

  setCategory(event: Event) {
    this.SearchFiltersService.setCategory(
      (event.target as HTMLSelectElement).value
    );
  }
  categories = [
    { value: 'EXP', title: 'Exposition' },
    { value: 'CINE', title: 'Cinéma' },
    { value: 'MUS', title: 'Musique' }
  ];
  constructor(public SearchFiltersService: SearchFiltersService) {}
}

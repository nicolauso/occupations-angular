import { Component, Input } from '@angular/core';
import { CardComponent } from '../card/card.component';
import { Event } from '../../interfaces/event';

@Component({
  selector: 'app-card-list',
  standalone: true,
  imports: [CardComponent],
  templateUrl: './card-list.component.html',
  styleUrl: './card-list.component.css'
})
export class CardListComponent {
  @Input() cardList: Event[] = [];
}

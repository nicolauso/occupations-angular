import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import 'flowbite-datepicker';
import 'flowbite/dist/datepicker.turbo.js';
import { SearchFiltersService } from '../../services/search-filters.service';

@Component({
  selector: 'app-date-picker',
  standalone: true,
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  templateUrl: './date-picker.component.html',
  styleUrl: './date-picker.component.css'
})
export class DatePickerComponent {
  @Input() startDate!: string;
  @Input() endDate!: string;

  onChangeSetStartDate(event: Event) {
    this.SearchFiltersService.setStartDate(
      (event.target as HTMLInputElement).value
    );
    console.log(
      'onChangeSetStartDate',
      (event.target as HTMLInputElement).value
    );
  }
  onChangeSetEndDate(event: Event) {
    this.SearchFiltersService.setEndDate(
      (event.target as HTMLInputElement).value
    );
    console.log('onChangeSetEndDate', (event.target as HTMLInputElement).value);
  }

  constructor(private SearchFiltersService: SearchFiltersService) {}
}

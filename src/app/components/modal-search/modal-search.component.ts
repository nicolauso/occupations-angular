import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SearchComponent } from '../search/search.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-modal-search',
  standalone: true,
  imports: [SearchComponent, CommonModule],
  templateUrl: './modal-search.component.html',
  styleUrl: './modal-search.component.css'
})
export class ModalSearchComponent {
  @Output() stateModal = new EventEmitter<boolean>();

  @Input() isOpen: boolean = true;

  setStateModal(value: boolean) {
    this.stateModal.emit(value);
  }
}

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Tab } from '../../../types/types';
import { RouterLink } from '@angular/router';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-tab',
  standalone: true,
  imports: [RouterLink, CommonModule],
  templateUrl: './tab.component.html',
  styleUrl: './tab.component.css'
})
export class TabComponent {
  @Input() isSelected!: boolean;
  @Input() tab!: Tab;

  @Output() tabSelected = new EventEmitter<void>();

  onTabSelected() {
    this.tabSelected.emit();
  }
}

import { Component } from '@angular/core';
import { TabComponent } from './tab/tab.component';
import { Tab } from '../../types/types';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-navbar-mobile',
  standalone: true,
  imports: [TabComponent, CommonModule],
  templateUrl: './navbar-mobile.component.html',
  styleUrl: './navbar-mobile.component.css'
})
export class NavbarMobileComponent {
  tabs: Tab[] = [
    {
      id: 0,
      icon: 'assets/icons/bell.svg',
      title: 'My Events',
      path: '/my-events'
    },
    {
      id: 1,
      icon: 'assets/icons/home.svg',
      title: 'Home',
      path: '/'
    },
    {
      id: 2,
      icon: 'assets/icons/profile.svg',
      title: 'My Profile',
      path: '/profile'
    }
  ];
  selectedTab: number = 1;

  getIsSelectedTab(tabId: number): boolean {
    return tabId === this.selectedTab;
  }
  getSelectedTab(tabId: number): Tab {
    const selectedTab = this.tabs.find((tab) => tab.id === tabId);
    return selectedTab || this.tabs[1]!;
  }

  onTabSelected(tabId: number): void {
    this.selectedTab = tabId;
  }
}

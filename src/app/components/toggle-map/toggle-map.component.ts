import { Component } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-toggle-map',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule],
  templateUrl: './toggle-map.component.html',
  styleUrl: './toggle-map.component.css'
})
export class ToggleMapComponent {
  toggleMapCard = new FormControl('');
  handleCheckboxChange(): boolean {
    return !this.toggleMapCard.value;
  }
}

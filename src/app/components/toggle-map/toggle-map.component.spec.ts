import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleMapComponent } from './toggle-map.component';

describe('ToggleMapComponent', () => {
  let component: ToggleMapComponent;
  let fixture: ComponentFixture<ToggleMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ToggleMapComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(ToggleMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleFreeComponent } from './toggle-free.component';

describe('ToggleFreeComponent', () => {
  let component: ToggleFreeComponent;
  let fixture: ComponentFixture<ToggleFreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ToggleFreeComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(ToggleFreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

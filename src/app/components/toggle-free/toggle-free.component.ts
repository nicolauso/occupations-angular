import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-toggle-free',
  standalone: true,
  imports: [],
  templateUrl: './toggle-free.component.html',
  styleUrl: './toggle-free.component.css'
})
export class ToggleFreeComponent {
  @Input() isFree!: boolean;

  @Output() isCheckIsFree = new EventEmitter<boolean>();

  checkIsFree(value: boolean) {
    this.isCheckIsFree.emit(value);
  }
}
